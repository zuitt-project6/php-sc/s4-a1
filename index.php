<?php require_once './code.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>S4 - A1</title>
</head>
<body style="text-align: center">
   <h1>Building</h1>
   <p><?= $building->getName() ?></p>
   <p><?= $building->getFloor() ?></p>
   <p><?= $building->getAddress() ?></p>
   <p><?= $building->setName("Astro Tower") ?></p>
  
   <h1>Condominium</h1>
   <p><?= $condo->getName() ?></p>
   <p><?= $condo->getFloor() ?></p>
   <p><?= $condo->getAddress() ?></p>
   <p><?= $condo->setName("Pogo Hotel") ?></p>

</body>
</html>