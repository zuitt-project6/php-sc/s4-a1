<?php 

class Building{
   protected $name;
   protected $address;
   protected $floor;

   function __construct($name, $address, $floor){
      $this->name = $name;
      $this->address = $address;
      $this->floor = $floor;
   }

   function setName($name){
      $this->name = $name;
      return "The building name has been changed to $this->name.";
   }

   function setAddress($address){
      $this->address = $address;
   }

   function setFloor($floor){
      $this->floor = $floor;
   }

   function getName(){
      return "The name of the building is $this->name.";
   }

   function getAddress(){
      return "The $this->name is located at $this->address.";
   }

   function getFloor(){
      return "The $this->name has $this->floor floors.";
   }
}

class Condominium extends Building{
   function setName($name){
      $this->name = $name;
      return "The condominium name has been changed to $this->name.";
   }

   function getName(){
      return "The name of the condominium is $this->name.";
   }

   function getAddress(){
      return "The $this->name is located at $this->address.";
   }

   function getFloor(){
      return "The $this->name has $this->floor floors.";
   }
}

$building = new Building("Astral Tower", "National Street, Quezon City", 15);
$condo = new Condominium('Condo Building', 'Somewhere down the road', 10);

/*
2 classes (building is parent, condo is child)
name, floor, address
this properties cannot be accessed or changed directly
create a constructor for the classes
both of the classes need to have getter and setter for all 3 properties
once both have getter and setter, display the name, floor, address of the 2 classes
and after that change both names of the classes
*/